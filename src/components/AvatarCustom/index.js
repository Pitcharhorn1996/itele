import React from 'react';
import { AvatarCustomStyled } from './styled';

export const AvatarCustom = (props) => {
  return (
    <AvatarCustomStyled {...props}>
      <div className="col_left">
        <div className="img" />
      </div>
    </AvatarCustomStyled>
  );
};
