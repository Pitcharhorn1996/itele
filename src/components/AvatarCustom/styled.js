import styled from 'styled-components';

export const AvatarCustomStyled = styled.div`
  /*===============================================
 Container 
===============================================*/

  /*===============================================
Theme 
===============================================*/
  display: flex;
  .col_left {
    .img {
      width: ${({ width }) => width ? width : "80px"};
      height: ${({ height }) => height ? height : "80px"};
      border-radius: 50%;
      border: 2px solid ${({ theme }) => theme.COLORS.WHITE_1};
      overflow: hidden;
      margin: auto;
      background-image: ${({ src }) => `url(${src})`};
      background-position: center;
      background-size: cover;
    }
  }
`;
