import styled from 'styled-components';

export const HeaderStatusStyled = styled.div`
  /*===============================================
 Container 
===============================================*/

  /*===============================================
Theme 
===============================================*/
  position: relative;
  background: ${({ theme }) => theme.COLORS.GREEN_1};
  padding: ${({ padding }) => (padding ? padding : '22px 15px')};
  text-align: center;
  font-family: prompt_medium;
  font-size: ${({ theme }) => theme.FONTS.SIZE_5};
  color: ${({ theme }) => theme.COLORS.WHITE_1};
`;
