import React from 'react';
import { HeaderStatusStyled } from './styled';

export const HeaderStatus = (props) => {
  const { children } = props;
  return <HeaderStatusStyled {...props}>{children}</HeaderStatusStyled>;
};
