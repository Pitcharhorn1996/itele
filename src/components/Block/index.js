import React from 'react';
import { BlockStyled } from './styled';
import cx from 'classnames';

export const Block = ({ theme_block_standard, theme_block_non_bottom,theme_block_grey, children }) => {
  const customClass = cx({
    theme_block_standard: theme_block_standard,
    theme_block_non_bottom: theme_block_non_bottom,
    theme_block_grey :theme_block_grey
  });
  return (
    <BlockStyled>
      <div className={customClass}>{children}</div>
    </BlockStyled>
  );
};
