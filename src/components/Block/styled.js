import styled from 'styled-components';

export const BlockStyled = styled.div`
  /*===============================================
 Container 
===============================================*/

  /*===============================================
Theme 
===============================================*/
  .theme_block_standard {
    background: ${({ theme }) => theme.COLORS.WHITE_1};
    border-radius: 8px;
    box-shadow: 0 2px 12px -5px rgba(0, 0, 0, 0.75);
    padding: 20px;
  }
  .theme_block_non_bottom {
    background: ${({ theme }) => theme.COLORS.WHITE_1};
    border-radius: 8px;
    box-shadow: 0 2px 12px -5px rgba(0, 0, 0, 0.75);
    padding: 20px 0px;
  }
  .theme_block_grey {
    background: ${({ theme }) => theme.COLORS.GREY_1};
    //border-radius: 8px;
    //box-shadow: 0 2px 12px -5px rgba(0, 0, 0, 0.75);
    padding: 10px 25px;
  }
`;
