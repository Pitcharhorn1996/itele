import React from 'react';
import { DivCustomStyled } from './styled';
import cx from 'classnames';

export const DivCustom = ({ theme_standard, text, ...props }) => {
  const customClass = cx({
    theme_standard: theme_standard,
  });
  return (
    <DivCustomStyled {...props}>
      <div className={customClass}>
        <div className="img" />
        <div className="text">{text}</div>
      </div>
    </DivCustomStyled>
  );
};
