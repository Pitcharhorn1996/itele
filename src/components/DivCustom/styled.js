import styled from 'styled-components';

export const DivCustomStyled = styled.div`
  /*===============================================
 Container 
===============================================*/

  /*===============================================
Theme 
===============================================*/
  .theme_standard {
    display: flex;
    border-radius: 8px;
    box-shadow: 0px 2px 12px -5px rgba(0, 0, 0, 0.5);
    min-height: 48px;
    .img {
      flex-shrink: 0;
      background-image: url(${({ src }) => src});
      width: 77px;
      background-repeat: no-repeat;
      background-size: cover;
      background-position: center;
  }
    }
    .text {
      width: 75%;
      padding: 15px 15px;
    }
  }
`;
