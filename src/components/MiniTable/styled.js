import styled from "styled-components"

export const MiniTableStyled = styled.div`
/*===============================================
 Container 
===============================================*/
    
/*===============================================
Theme 
===============================================*/
    .theme_standard {
        .title {
            font-weight: 400;
            font-size: ${({ theme }) => theme.FONTS.SIZE_2};
            color: ${({ theme }) => theme.COLORS.GREY_7};
            margin-bottom: 3px;
        }
        .row_layer {
            display: flex;
            justify-content: space-between;
            .row_layer_l {
                font-weight: 400;
                font-size: ${({ theme }) => theme.FONTS.SIZE_2};
                color: ${({ theme }) => theme.COLORS.GREY_7};
            }
            .row_layer_r {
                font-weight: 400;
                font-size: ${({ theme }) => theme.FONTS.SIZE_2};
                color: ${({ theme }) => theme.COLORS.PURPLE_1};
                padding-left: 10px;
                min-width: 70px;
                display: flex;
                flex-shrink: 0;
                justify-content: flex-end;
                margin-bottom: 7px;
            }
            .row_layer_s {
                font-weight: 700;
                font-size: ${({ theme }) => theme.FONTS.SIZE_2};
                color: ${({ theme }) => theme.COLORS.GREEN_1};
                padding-left: 10px;
                min-width: 70px;
                display: flex;
                flex-shrink: 0;
                justify-content: flex-end;
            }
        }
        .line {
            border-bottom: 1px solid ${({ theme }) => theme.COLORS.GREY_6};
            margin: 12px 0px;
        }
        .grand_layer {
            display: flex;
            justify-content: space-between;
        }
    }
`