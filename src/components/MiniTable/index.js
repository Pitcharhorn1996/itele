import React from 'react'
import { MiniTableStyled } from './styled'
import cx from 'classnames';

export const MiniTable = ({ theme_standard, data, title }) => {
    const customClass = cx({
        "theme_standard": theme_standard
    })
    return (
        <MiniTableStyled>
            <div className={customClass}>
                <div className="title">{title}</div>
                {
                    data && data.map((e, i) => (
                        <div className="row_layer">
                            <div className="row_layer_l">
                                - {e.detail}
                            </div>
                            <div className="row_layer_r">
                                ฿{e.price}
                            </div>
                        </div>
                    ))
                }
                <div className="line" />
                <div className="row_layer">
                    <div className="row_layer_l">
                        รวมทั้งหมด / Total
                    </div>
                    <div className="row_layer_s">
                        ฿585.20
                    </div>
                </div>
            </div>
        </MiniTableStyled>
    )
}