import styled from 'styled-components';

export const BoxDoctorStyled = styled.div`
  /*===============================================
 Container 
===============================================*/

  /*===============================================
Theme 
===============================================*/
  border-bottom: 2px solid ${({ theme }) => theme.COLORS.GREEN_2};
  background: ${({ theme }) => theme.COLORS.WHITE_1};
  padding: 15px;
  .box_top {
    display: flex;
    position: relative;
    margin-bottom: 30px;
    .box_top_left {
      position: absolute;
      top: -53px;
    }
    .box_top_right {
      margin-left: 133px;
      font-family: prompt_medium;
      font-size: ${({ theme }) => theme.FONTS.SIZE_4};
      color: ${({ theme }) => theme.COLORS.BLACK_1};
    }
  }
  .box_bottom {
    font-size: ${({ theme }) => theme.FONTS.SIZE_4};
    color: ${({ theme }) => theme.COLORS.BLACK_1};
    line-height: 1.5;
  }
`;
