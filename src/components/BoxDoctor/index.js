import React from 'react';
import { BoxDoctorStyled } from './styled';
import { AvatarCustom } from 'components';
import { iconImages } from 'assets';

export const BoxDoctor = () => {
  return (
    <BoxDoctorStyled>
      <div className="box_top">
        <div className="box_top_left">
          <AvatarCustom src={iconImages['avatar.png']} />
        </div>
        <div className="box_top_right">Doctor’s Advice</div>
      </div>
      <div className="box_bottom">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna wirl
      </div>
    </BoxDoctorStyled>
  );
};
