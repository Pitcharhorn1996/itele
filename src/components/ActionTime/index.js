import React from 'react';
import { ActionTimeStyled } from './styled';

export const ActionTime = () => {
  return (
    <ActionTimeStyled>
      <div className="title">Time</div>
      <div className="row">
        <div className="col left">
          <button theme_full_width className="active">All day</button>
        </div>
        <div className="col right">
          <button theme_full_width>Morning</button>
        </div>
      </div>
      <div className="row">
        <div className="col left">
          <button theme_full_width>All day</button>
        </div>
        <div className="col right">
          <button theme_full_width>Morning</button>
        </div>
      </div>
    </ActionTimeStyled>
  );
};
