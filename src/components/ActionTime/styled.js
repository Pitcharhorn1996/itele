import styled from 'styled-components';

export const ActionTimeStyled = styled.div`
  /*===============================================
 Container 
===============================================*/

  /*===============================================
Theme 
===============================================*/
  .title {
    font-family: prompt_medium;
  }
  .row {
    display: flex;
    margin-top: 16px;
    .col {
      width: 50%;
      &.left {
        padding-right: 8px;
      }
      &.right {
        padding-left: 8px;
      }
      button {
        color: ${({ color, theme }) => (color ? color : theme.COLORS.BLACK_1)};
        width: 100%;
        padding: 12px;
        border: 1px solid ${({ theme }) => theme.COLORS.GREEN_1};
        border-radius: 8px;
        box-shadow: 0px 2px 12px -5px rgba(0, 0, 0, 0.5);
        font-size: ${({ theme }) => theme.FONTS.SIZE_4};
        font-family: prompt_medium;
        &.active {
          background: ${({ theme }) => theme.COLORS.GREEN_1};
          color: ${({ color, theme }) =>
            color ? color : theme.COLORS.WHITE_1};
        }
      }
    }
  }
`;
