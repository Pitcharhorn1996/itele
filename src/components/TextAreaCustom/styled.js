import styled from "styled-components"
 
export const TextAreaCustomStyled = styled.div`
/*===============================================
 Container 
===============================================*/
    
/*===============================================
Theme 
===============================================*/
    .theme_standard {
        .text_area {
            width: 97.5%;
            padding: 5px;
            min-height: 127px;
            border-radius: 8px;
            border: none;
            background: ${({ theme }) => theme.COLORS.GREY_1};
            font-weight: 400;
            font-size: ${({ theme }) => theme.FONTS.SIZE_3};
            color: ${({ theme }) => theme.COLORS.BLACK_1};
        }
    }
`