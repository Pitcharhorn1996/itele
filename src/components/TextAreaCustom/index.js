import React from 'react'
import { TextAreaCustomStyled } from './styled'
import cx from 'classnames';

export const TextAreaCustom = ({ theme_standard }) => {
    const customClass = cx({
        "theme_standard": theme_standard
    })
    return (
        <TextAreaCustomStyled>
            <div className={customClass}>
                <textarea className="text_area" />
            </div>
        </TextAreaCustomStyled>
    )
}