import React from 'react'
import { AaPlatformFunctionStyled } from './styled'
import cx from 'classnames';

export const AaPlatformFunction = ({ theme_standard }) => {
    const customClass = cx({
        "theme_standard": theme_standard
    })
    return (
        <AaPlatformFunctionStyled>
            <div className={customClass}>
                test dddd
            </div>
        </AaPlatformFunctionStyled>
    )
}