import styled from 'styled-components';

export const InputCustomStyled = styled.div`
  /*===============================================
 Container 
===============================================*/

  /*===============================================
Theme 
===============================================*/
  .theme_standard {
    display: flex;
    .label_wrap {
      flex-shrink: 0;
      display: flex;
      margin-right: 10px;
      img {
        width: 20px;
        margin-right: 5px;
      }
    }
    input {
      width: 100%;
      min-width: 50px;
      border: none;
      font-family: prompt_light;
      border-bottom: 1px solid
        ${({ theme, errors }) =>
          errors ? theme.COLORS.RED_1 : theme.COLORS.GREY_2};
      font-weight: 500;
      font-size: ${({ theme }) => theme.FONTS.SIZE_3};
      color: ${({ theme }) => theme.COLORS.BLACK_1};
      &::placeholder {
        width: 100%;
        color: rgba(0, 0, 0, 0.38);
      }
    }
  }
  .theme_round {
    display: flex;
    padding: 10px 20px;
    border-radius: 20px;
    border: 1px solid
      ${({ theme, errors }) =>
        errors ? theme.COLORS.RED_1 : theme.COLORS.GREY_3};
    background: ${({ theme }) => theme.COLORS.GREY_4};
    .label_wrap {
      flex-shrink: 0;
      display: flex;
      margin-right: 10px;
      img {
        width: 20px;
        margin-right: 5px;
      }
    }
    input {
      width: 100%;
      border: none;
      font-weight: 400;
      font-family: prompt_light;
      font-size: ${({ theme }) => theme.FONTS.SIZE_4};
      color: ${({ theme }) => theme.COLORS.BLACK_1};
      background: ${({ theme }) => theme.COLORS.GREY_4};
      &::placeholder {
        width: 100%;
        color: rgba(0, 0, 0, 0.38);
      }
    }
  }
`;
