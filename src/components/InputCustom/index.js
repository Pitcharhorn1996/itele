import React from 'react';
import { InputCustomStyled } from './styled';
import cx from 'classnames';

export const InputCustom = ({
  theme_standard,
  theme_round,
  placeHolder,
  src,
  label,
  type = 'text',
  input,
  meta: { touched, error },
}) => {
  const customClass = cx({
    theme_standard: theme_standard,
    theme_round: theme_round,
  });

  return (
    <InputCustomStyled errors={touched && error}>
      <div className={customClass}>
        {label && (
          <div className="label_wrap">
            {src && <img src={src} alt="doctor" />}
            <div className="label_layout">{label}</div>
          </div>
        )}
        <input {...input} type={type} placeholder={placeHolder} />
      </div>
    </InputCustomStyled>
  );
};
