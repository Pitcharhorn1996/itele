import styled from 'styled-components';

export const DayPickkerStyled = styled.div`
  /*===============================================
 Container 
===============================================*/

  /*===============================================
Theme 
===============================================*/
  width: 100%;
  .DayPicker {
    width: 100%;
  }
  .DayPicker-wrapper {
    border: none;
    box-shadow: unset;
    outline: unset;
  }
  .DayPicker-Day--selected:not(.DayPicker-Day--disabled):not(.DayPicker-Day--outside) {
    background: ${({ theme }) => theme.COLORS.GREEN_1};
    border: none;
  }
  .DayPicker-Day {
    padding: 8px 10px;
    border: none;
    box-shadow: unset;
    outline: unset;
  }
`;
