import React, { useState } from 'react';
import { DayPickkerStyled } from './styled';
import DayPicker from 'react-day-picker';
import 'react-day-picker/lib/style.css';

export const DayPickker = ({ ...props }) => {
  const [selectedDay, setSelectedDay] = useState();

  const handleDayClick = (day) => {
    setSelectedDay(day);
  };

  return (
    <DayPickkerStyled {...props}>
      <DayPicker onDayClick={handleDayClick} selectedDays={selectedDay} />
    </DayPickkerStyled>
  );
};
