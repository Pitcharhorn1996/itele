import React from 'react';
import { ButtonCircleStyled } from './styled';

export const ButtonCircle = ({ theme_standard, ...props }) => {
  const { imgNode, text } = props;
  return (
    <ButtonCircleStyled {...props}>
      {imgNode}
      <div className="label">{text}</div>
    </ButtonCircleStyled>
  );
};
