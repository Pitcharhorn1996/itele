import styled from 'styled-components';

export const ButtonCircleStyled = styled.div`
  /*===============================================
 Container 
===============================================*/

  /*===============================================
Theme 
===============================================*/
  width: ${({ diameter }) => (diameter ? diameter : '68px')};
  height: ${({ diameter }) => (diameter ? diameter : '68px')};
  border-radius:  ${({ radius }) => (radius ? radius : '50%')};
  background: ${({ bg }) => (bg ? bg : 'rgba(19, 19, 19, 0.75)')};
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  .label {
    font-size: ${({ theme }) => theme.FONTS.SIZE_3};
    text-align: center;
    color: ${({ color, theme }) => (color ? color : theme.COLORS.WHITE_1)};  
  }
`;
