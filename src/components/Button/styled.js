import styled from 'styled-components';

export const ButtonStyled = styled.div`
  /*===============================================
 Container 
===============================================*/
width: 100%;
  /*===============================================
Theme 
===============================================*/
  .theme_full_width {
    button {
      background: ${({ bg, theme }) => (bg ? bg : theme.COLORS.GREEN_1)};
      color: ${({ color, theme }) => (color ? color : theme.COLORS.WHITE_1)};
      width: 100%;
      padding: 12px;
      border: none;
      border-radius: 8px;
      box-shadow: 0px 2px 12px -5px rgba(0, 0, 0, 0.5);
      font-size: ${({ theme }) => theme.FONTS.SIZE_4};
      font-family: prompt_medium;
    }
  }
  .theme_green {
    width: 100%;
    button {
      background: ${({ bg, theme }) => (bg ? bg : theme.COLORS.GREEN_1)};
      color: ${({ color, theme }) => (color ? color : theme.COLORS.WHITE_1)};
      width: 100%;
      padding: 10px;
      border: none;
      border-radius: 16px;
      box-shadow: 0px 2px 12px -5px rgba(0, 0, 0, 0.5);
      font-size: ${({ theme }) => theme.FONTS.SIZE_4};
      font-family: prompt_medium;
      font-weight: 700;
    }
  }
  .theme_normal {
    width: 100%;
    button {
      background: ${({ bg, theme }) => (bg ? bg : theme.COLORS.GREEN_1)};
      color: ${({ color, theme }) => (color ? color : theme.COLORS.WHITE_1)};
      width: 100%;
      padding: 10px;
      border: none;
      border-radius: 16px;
      box-shadow: 0px 2px 12px -5px rgba(0, 0, 0, 0.5);
      font-size: ${({ theme }) => theme.FONTS.SIZE_4};
      font-family: prompt_medium;
      font-weight: 400;
    }
  }
`;
