import React from 'react';
import { ButtonStyled } from './styled';
import cx from 'classnames';

export const Button = ({ theme_full_width, theme_green, theme_normal, children, ...props }) => {
  const customClass = cx({
    theme_full_width: theme_full_width,
    theme_green: theme_green,
    theme_normal:theme_normal,
  });
  return (
    <ButtonStyled {...props}>
      <div className={customClass}>
        <button>{children}</button>
      </div>
    </ButtonStyled>
  );
};
