import styled from 'styled-components';

export const CheckBoxCustomStyled = styled.div`
  /*===============================================
 Container 
===============================================*/
  /*===============================================
Theme 
===============================================*/
  .theme_standard {
    display: flex;
    align-items: center;
    cursor: pointer;
    img {
      padding-top: 2px;
      width: 19px;
      height: 19px;
    }
    .status {
      margin-left: 8px;
      border-radius: 8px;
      background: ${({ theme, bgColor }) =>
        bgColor ? bgColor : theme.COLORS.BLUE_4};
      color: ${({ theme, color }) => (color ? color : theme.COLORS.WHITE_1)};
      font-family: prompt_regular;
      font-size: ${({ theme }) => theme.FONTS.SIZE_1};
      padding: 6px 8px;
      span {
        margin-right: 4px;
      }
      img {
        width: 12px;
        padding-bottom: 2px;
      }
      .text {
        padding-top: 2px;
        padding-left: 4px;
      }
    }
    .text_detail {
      padding-left: 6px;
      color: ${({ theme, text_detail_color }) => (text_detail_color ? theme.COLORS.WHITE_1 : theme.COLORS.BLACK_2)};
      font-size: ${({ theme }) => theme.FONTS.SIZE_2};
    }
  }
`;
