import React, { useState, useEffect } from 'react';
import { CheckBoxCustomStyled } from './styled';
import cx from 'classnames';
import { iconImages } from 'assets';

export const CheckBoxCustom = ({
  theme_standard,
  text,
  status,
  icon,
  color,
  bgColor,
  onChange,
  text_detail_color,
  intialValue,
  input,
  meta: { touched, error },
}) => {
  const customClass = cx({
    theme_standard: theme_standard,
  });
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    setIsActive(input.value);
  }, [input]);

  const handleClick = () => {
    let cloneIsActive = !isActive;
    setIsActive(cloneIsActive);
    input.onChange(cloneIsActive)
    onChange && onChange(cloneIsActive);
  };

  return (
    <CheckBoxCustomStyled
      color={color}
      bgColor={bgColor}
      text_detail_color={text_detail_color}
    >
      <div className={customClass} onClick={handleClick}>
        {/* <img
          src={
            isActive
              ? iconImages['checkbox_active.png']
              : iconImages['checkbox.png']
          }
          alt="ddc"
        /> */}
        {status && (
          <div className="status">
            {icon && (
              <span>
                <img src={icon} alt="ddc" />
              </span>
            )}
            {status}
          </div>
        )}
        <div className="text_detail">{text}</div>
      </div>
    </CheckBoxCustomStyled>
  );
};
