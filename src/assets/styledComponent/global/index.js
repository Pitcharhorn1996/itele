import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
	body {
		margin: 0;
		font-size: 16px;
		font-family: prompt_light;
		input {
			outline: unset;
		}
	}
`;
