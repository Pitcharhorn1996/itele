function importAll(r) {
    let images = {}
    r.keys().map((item, index) => {
        return images[item.replace('./', '')] = r(item)
    })
    return images
}

const iconImages = importAll(require.context('./icon', false, /\.(png|jpe?g|svg)$/))

export {
    iconImages,
}