import axios from 'axios';
import { BASE_API, BASE_PATH_API } from './apiConfig';

const getConfig = (token) => {
  const config = {
    baseURL: BASE_API + BASE_PATH_API,
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  };
  return config;
};

const getConfigFormData = (token, callback) => {
  const config = {
    baseURL: BASE_API + BASE_PATH_API,
    headers: {
      'Content-Type': 'multipart/form-data',
      Authorization: `Bearer ${token}`,
    },
    onUploadProgress: function (progressEvent) {
      let percentCompleted = Math.round(
        (progressEvent.loaded * 100) / progressEvent.total
      );
      console.log('upload percent', percentCompleted);
      callback && callback(percentCompleted);
    },
  };
  return config;
};

const axiosSuccess = (result) => {
  return result.data;
};

const axiosError = (error) => {
  return error.response.data;
};

const axiosService = (type, url, params, callback) => {
  // session storage
  let config = getConfig(localStorage.getItem('ddc_token'));
  let configFormData = getConfigFormData(
    localStorage.getItem('ddc_token'),
    callback
  );
  switch (type) {
    case 'get':
      return axios.get(url, config).then(axiosSuccess).catch(axiosError);
    case 'post':
      return axios
        .post(url, params, config)
        .then(axiosSuccess)
        .catch(axiosError);
    case 'put':
      return axios
        .put(url, params, config)
        .then(axiosSuccess)
        .catch(axiosError);
    case 'delete':
      return axios
        .delete(url, { ...config, data: params })
        .then(axiosSuccess)
        .catch(axiosError);
    case 'post_formdata':
      return axios
        .post(url, params, configFormData)
        .then(axiosSuccess)
        .catch(axiosError);
    default:
      return false;
  }
};

const getConfigCustom = (baseUrl, token) => {
  console.log(baseUrl);
  const config = {
    baseURL: baseUrl,
    headers: {
      'Content-Type': 'application/json',
      // Authorization: `Bearer ${token}`,
    },
  };
  return config;
};

const axiosServiceCustom = (type, baseUrl, url) => {
  // session storage
  let config = getConfigCustom(baseUrl, localStorage.getItem('ddc_token'));
  switch (type) {
    case 'get':
      return axios.get(url, config).then(axiosSuccess).catch(axiosError);
    default:
      return false;
  }
};

export default {
  get: (url, params) => axiosService('get', url, params),
  post: (url, params) => axiosService('post', url, params),
  put: (url, params) => axiosService('put', url, params),
  delete: (url, params) => axiosService('delete', url, params),
  post_formdata: (url, params, callback) =>
    axiosService('post_formdata', url, params, callback),

  get_base: (baseUrl, url, params) =>
    axiosServiceCustom('get', baseUrl, url, params),
};
