import apiService from '../apiService';
// import { BASE_API_GISDA } from '../apiConfig';

const apiPath = '/api';

export const userService = {
  // member DB
  GET_MEMBER_LIST: () => {
    return apiService.get(`${apiPath}/member`);
  },
  GET_MEMBER_DETAIL: (id) => {
    return apiService.get(`${apiPath}/member/detail/${id}`);
  },
  POST_MEMBER_ADD: (params) => {
    return apiService.post(`${apiPath}/member/add`, params);
  },
  PUT_MEMBER_UPDATE: (id, params) => {
    return apiService.put(`${apiPath}/member/update/${id}`, params);
  },
  DELETE_MEMBER_DETAIL: (id) => {
    return apiService.delete(`${apiPath}/member/delete/${id}`);
  },

  POST_VISITS: (params) => {
    return apiService.post(`${apiPath}/visits`, params);
  },
};
