import React from 'react';
import { store, persistor } from 'reducers';
import { ThemeProvider } from 'styled-components';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import theme from 'assets/styledComponent/theme.json';
import { GlobalStyle } from 'assets';
import { BrowserRouter as Router } from 'react-router-dom';
import Routes from 'routes';
import { Notification } from 'components';

function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <ThemeProvider theme={theme}>
          <GlobalStyle />
          <Notification />
          <Router>
            <Routes />
          </Router>
        </ThemeProvider>
      </PersistGate>
    </Provider>
  );
}

export default App;
