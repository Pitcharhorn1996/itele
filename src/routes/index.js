import React from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import { PageMainLayout } from 'mainLayouts';

const indexRoutes = [
  { path: '/', exact: false, name: 'page', component: PageMainLayout },
];

class Routes extends React.Component {
  render() {
    return (
      <>
        <Switch>
          {indexRoutes.map((prop, key) => {
            return (
              <Route
                exact={prop.exact}
                path={prop.path}
                component={prop.component}
                key={key}
              />
            );
          })}
        </Switch>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  isLoading: state.isLoading,
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Routes);
