import { ROUTE_PATH } from 'helpers';
// import PatientCallingContainer from 'containers/PatientCallingContainer';
// import PatientFinishContainer from 'containers/PatientFinishContainer';
// import PatientRequestContainer from 'containers/PatientRequestContainer';
// import PatientWaitingContainer from 'containers/PatientWaitingContainer';
// import PatientConnectingContainer from 'containers/PatientConnectingContainer';
import PatientRegisterContainer from 'containers/PatientRegisterContainer';
import PatientVideoCallContainer from 'containers/PatientVideoCallContainer';
import PatientRatingContainer from 'containers/PatientRatingContainer';
import PatientSummaryContainer from 'containers/PatientSummaryContainer';
import PatientEndProcessContainer from 'containers/PatientEndProcessContainer';
import ConditionContainer from 'containers/ConditionContainer';
import OpenCamContainer from 'containers/OpenCamContainer';

var pageRoutes = [
  {
    keyIndex: 1,
    path: '/',
    exact: true,
    name: 'register',
    component: PatientRegisterContainer,
  },
  {
    keyIndex: 1,
    path: '/condition',
    exact: true,
    name: 'register',
    component: ConditionContainer,
  },
  {
    keyIndex: 1,
    path: '/opencamera',
    exact: true,
    name: 'register',
    component: OpenCamContainer,
  },
  {
    keyIndex: 1,
    path: ROUTE_PATH.VIDEO_CALL,
    exact: true,
    name: 'register',
    component: PatientVideoCallContainer,
  },
  {
    keyIndex: 1,
    path: ROUTE_PATH.VIDEO_CALL,
    exact: true,
    name: 'register',
    component: PatientVideoCallContainer,
  },
  {
    keyIndex: 1,
    path: ROUTE_PATH.VIDEO_CALL,
    exact: true,
    name: 'register',
    component: PatientVideoCallContainer,
  },
  {
    keyIndex: 1,
    path: ROUTE_PATH.PATIENT_RATING,
    exact: true,
    name: 'register',
    component: PatientRatingContainer,
  },
  {
    keyIndex: 1,
    path: ROUTE_PATH.PATIENT_SUMMARY,
    exact: true,
    name: 'register',
    component: PatientSummaryContainer,
  },
  {
    keyIndex: 1,
    path: ROUTE_PATH.PATIENT_THANK,
    exact: true,
    name: 'register',
    component: PatientEndProcessContainer,
  },

  
  // {
  //   keyIndex: 1,
  //   path: ROUTE_PATH.PATIENT_CALLING,
  //   exact: true,
  //   name: 'register',
  //   component: PatientCallingContainer,
  // },
  // {
  //   keyIndex: 1,
  //   path: ROUTE_PATH.PATIENT_FINIST,
  //   exact: true,
  //   name: 'register',
  //   component: PatientFinishContainer,
  // },
  // {
  //   keyIndex: 1,
  //   path: ROUTE_PATH.PATIENT_REQUEST,
  //   exact: true,
  //   name: 'register',
  //   component: PatientRequestContainer,
  // },
  // {
  //   keyIndex: 1,
  //   path: ROUTE_PATH.PATIENT_WAITNG,
  //   exact: true,
  //   name: 'register',
  //   component: PatientWaitingContainer,
  // },
  // {
  //   keyIndex: 1,
  //   path: ROUTE_PATH.PATIENT_CONNECTING,
  //   exact: true,
  //   name: 'register',
  //   component: PatientConnectingContainer,
  // },
];

export default pageRoutes;
