import React from 'react';
import { reduxForm, Field } from 'redux-form';
import { PatientRegisterFormStyled } from './styled';
import { VALIDATE } from 'helpers';
import { InputCustom, CheckBoxCustom } from 'components';

class PatientRegisterForm extends React.Component {
  state = {};

  render() {
    return (
      <PatientRegisterFormStyled>
        <div className="group_text">
          <div className="label_layer">ชื่อ</div>
          <div>
            <Field
              theme_round
              name="fname"
              placeHolder="กรุณากรอกชื่อ"
              validate={VALIDATE.REQUIRE}
              component={InputCustom}
            />
          </div>
        </div>
        <div className="group_text">
          <div className="label_layer">นามสกุล</div>
          <div>
            <Field
              theme_round
              name="lname"
              placeHolder="กรุณากรอกนามสกุล"
              validate={VALIDATE.REQUIRE}
              component={InputCustom}
            />
          </div>
        </div>
        <div className="group_text">
          <div className="label_layer">วันเดือนปีเกิด</div>
          <div>
            <Field
              theme_round
              type="date"
              name="birth_date"
              placeHolder="วันเดือนปีเกิด"
              validate={VALIDATE.REQUIRE}
              component={InputCustom}
            />
          </div>
        </div>
        <div className="group_text">
          <div className="label_layer">เลขบัตรประชาชน</div>
          <div>
            <Field
              theme_round
              type="number"
              name="cid"
              placeHolder="กรุณากรอกเลขบัตรประชาชน"
              validate={VALIDATE.ID_CARD}
              component={InputCustom}
            />
          </div>
        </div>
        <div className="group_text">
          <div className="label_layer">เบอร์โทรศัพท์</div>
          <div>
            <Field
              theme_round
              name="phone"
              type="number"
              placeHolder="กรุณากรอกเบอร์โทรศัพท์"
              validate={VALIDATE.REQUIRE}
              component={InputCustom}
            />
          </div>
        </div>
        <div className="accept_layer">
          <Field
            theme_standard
            name="is_approved"
            validate={VALIDATE.REQUIRE}
            component={CheckBoxCustom}
          />
          <div className="text_b1">ยอมรับเงื่อนไขในการรับบริการ</div>
          <div className="text_b2">ดูรายละเอียดเงื่อนไข</div>
        </div>
      </PatientRegisterFormStyled>
    );
  }
}

export default reduxForm({
  form: 'PatientRegisterForm', // a unique identifier for this form
  enableReinitialize: true,
})(PatientRegisterForm);
