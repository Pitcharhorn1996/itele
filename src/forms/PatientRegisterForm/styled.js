import styled from "styled-components"
 
export const PatientRegisterFormStyled = styled.div`
/*===============================================
 Container 
===============================================*/

/*===============================================
Theme 
===============================================*/
   .group_text {
       margin-bottom: 12px;
       .label_layer {
        font-weight: 400;
        font-size: ${({ theme }) => theme.FONTS.SIZE_4};
        color: ${({ theme }) => theme.COLORS.BLACK_1};
        margin-bottom: 2px; 
       }
   }
   .accept_layer {
       margin-top: 13px;
       margin-bottom: 33px;
       display: flex;
       font-weight: 700;
       font-size: ${({ theme }) => theme.FONTS.SIZE_3};
       color: ${({ theme }) => theme.COLORS.BLACK_1};
       .checkbox {
           width: 19px;
           height: 19px;
           margin-right: 8px;
       }
       .text_b1 {
           margin-right: 3px;
       }
       .text_b2 {
        color: ${({ theme }) => theme.COLORS.GREEN_1};
        border-bottom: 1px solid ${({ theme }) => theme.COLORS.GREEN_1};
       }
   }
`