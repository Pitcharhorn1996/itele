import styled from "styled-components"
 
export const TitleConditionStyled = styled.div`
/*===============================================
 Container 
===============================================*/

/*===============================================
Theme 
===============================================*/
   
   .title_layer {
       display: flex;
       font-weight: 700;
       padding-left: 20px;
       .text_b1 {
        color: 	#551A8B;
       }
   }
`