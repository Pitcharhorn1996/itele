import React from 'react';
import { reduxForm } from 'redux-form';
import { TitleConditionStyled } from './styled';

class TitleConditionForm extends React.Component {
  state = {};

  render() {
    return (
      <TitleConditionStyled>
        <div className="title_layer">
          <div className="text_b1">ข้อกำหนดและเงื่อนไขของบริการ</div>
        </div>
      </TitleConditionStyled>
    );
  }
}

export default reduxForm({
  form: 'TitleCoditionForm'
  //enableReinitialize: true,
})(TitleConditionForm);
