import React from 'react';
import { reduxForm } from 'redux-form';
import { DetailConditionStyled } from './styled';


class DetailCondition extends React.Component {
  state = {};

  render() {
    return (
      <DetailConditionStyled>
        <div className="body">
            <p className="text_1">หากผู้ใช้บริการที่มีอาการเจ็บป่วยรุนแรงเฉียบพลัน อาการสาหัส หรือมีอาการที่ต้องการรักษาพยาบาลแบบเร่งด่วน</p>
            <p className="text_2"><strong>โปรดติดต่อเจ้าหน้าที่โรงพยาบาล หรือ เรียกรถพยาบาลฉุกเฉิน</strong></p>
            <div className="text_1">
                <p className="text_1">iTele (Virtual Hospital) ให้บริการโดยบริษัท อินไวเทรส จำกัด (ซึ่งต่อไปนี้ใช้แทนคำว่า "อินไวเทรส" "เรา" "พวกเรา") โปรดอ่านข้อกำหนดและเงื่อนไขขงบริการ iTele (Virtual Hospital)(ข้อกำหนดและเงื่อนไข) โดยละเอียด การเข้าใช้บริการนี้ ถือว่าท่านตกลงที่จะปฏิบัติตามข้อกำหนดและเงื่อนไข รวมถึงข้อปฏิบัติต่างๆ ที่เกี่ยวข้องกับข้อกำหนดและเงื่อนไขต่อไปนี้</p>
                <p className="text_1">1. บริการ</p>
                <p className="text_1">1.1 บริการ บริการ iTele (Virtual Hospital) (บริการ) ที่ให้บริการผ่านเว็บไซต์ของเรา (เว็บไซต์) หรือแอปพลิเคชั่นของเรา (แอปพลิเคชั่น) ซึ่งประกอบด้วยบริการดังต่อไปนี้</p>
                <p className="text_1">(ก) บริการตรวจรักษาและให้คำแนะนำเกี่ยวกับโรคทั่วไป หรือการตรวจวินิจฉัยอาการเบื้องต้นโดยแพทย์ผู้เชี่ยวชาญ (ต่อไปนี้จะใช้คำแทนว่า "แพทย์") ด้วยการสื่อสารทางไกลทางระบบวีดีโอคอนเฟอเรนซ์ ผ่านเว็บไซต์พรือแอปพลิเคชั่นของเรา</p>
            </div>
        </div>
      </DetailConditionStyled>
    );
  }
}

export default reduxForm({
  form: 'DetailCondition'
  //enableReinitialize: true,
})(DetailCondition);