import styled from "styled-components"
 
export const DetailConditionStyled = styled.div`
/*===============================================
 Container 
===============================================*/

/*===============================================
Theme 
===============================================*/
   
.body {
    height: 90%;
    background: ${({ theme }) => theme.COLORS.GREY_1};
    .header_layout {
      padding: 10px 0px;
      font-weight: 600;
      font-size: ${({ theme }) => theme.FONTS.SIZE_5};
      color: ${({ theme }) => theme.COLORS.GREEN_1};
      display: flex;
      justify-content: center;
    }
    .text_1 {
      font-weight: 600;
      font-size: ${({ theme }) => theme.FONTS.SIZE_2};
      color: #A0A0A0;
    }
    .text_2 {
        font-weight: 1000;
        font-size: ${({ theme }) => theme.FONTS.SIZE_2};
        color: #404040;
      }
    .body_layout {
      font-weight: 700;
      font-size: ${({ theme }) => theme.FONTS.SIZE_4};
      color: #808080;
      display: flex;
      justify-content: center;
      
      }
    }
    .footer_layout {
      padding: 0px 20px;
    }
  }
   
`