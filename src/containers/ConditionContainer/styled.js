import styled from 'styled-components';

export const ConditionContainerStyled = styled.div`
  /*===============================================
 Container 
===============================================*/
  height: 100%;
  /*===============================================
Theme 
===============================================*/
  
  .header_layout {
    text-align: center;
    margin-bottom: 10px;
    padding: 0px 5px;
      .logo {
          width: 152px
      }
  }

  .body_layout {
    height: calc(100% - 175px);
    overflow: auto;
    margin-bottom: 20px;
  }

  .footer_layout {
    padding: 30px 20px;
  }

`;
