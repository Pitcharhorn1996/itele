import React from 'react';
import { connect } from 'react-redux';
import { ConditionContainerStyled } from './styled';
import { iconImages } from 'assets';
import { Button,Block } from 'components';
import { TitleCondition, DetailCondition } from 'forms';
import { ROUTE_PATH} from 'helpers';

class ConditionContainer extends React.Component {
  state = {};
  handleClickSuccess = async () => {
    
        this.props.history.push(ROUTE_PATH.LOGIN);
      
  };

  render() {
    return (
      <ConditionContainerStyled>
        {/* ******************************* logo header ******************************** */}
          <div className="header_layout">
            <img className="logo" src={iconImages['logo.png']} alt="logo" />
          </div>
        {/* ******************************* bady text detail ******************************** */}
          <TitleCondition />
          <div className="body_layout">
            <Block theme_block_grey>
              <DetailCondition/>
            </Block>
          </div>
        {/* ******************************* button ******************************** */}
          <div className="footer_layer">
            <div onClick={this.handleClickSuccess}>
              <Button theme_green>ยอมรับ</Button>
            </div>
          </div>

      </ConditionContainerStyled>
    );
  }
}


export default connect(
)(ConditionContainer);
