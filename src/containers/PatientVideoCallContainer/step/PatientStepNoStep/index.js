import React from 'react';
import { connect } from 'react-redux';
import { PatientStepNoStepStyled } from './styled';
import { iconImages } from 'assets';
import { setReduxUserAuth } from 'actions';

class PatientStepNoStep extends React.Component {
  state = {};

  render() {
    return (
      <PatientStepNoStepStyled>
        <div className="header_layout">
          <img
            className="logo_layout"
            src={iconImages['logo.png']}
            alt="logo"
          />
        </div>
        <div className="body_layout"></div>
        <div className="footer_layout"></div>
      </PatientStepNoStepStyled>
    );
  }
}

const mapStateToProps = (state) => ({
  authRedux: state.auth,
});

const mapDispatchToProps = (dispatch) => ({
  setReduxUserAuth: (data) => dispatch(setReduxUserAuth(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PatientStepNoStep);
