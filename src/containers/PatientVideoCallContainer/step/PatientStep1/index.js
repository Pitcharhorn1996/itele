import React from 'react';
import { connect } from 'react-redux';
import { PatientStep1Styled } from './styled';
// import { userService } from 'apiService';
import { iconImages } from 'assets';
import { setReduxUserAuth } from 'actions';
import { AvatarCustom } from 'components';

// const liff = window.liff;

class PatientStep1 extends React.Component {
  state = {
    message: '...',
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.data !== prevState.data) {
      return { data: nextProps.data };
    } else return null;
  }

  componentDidMount() {}

  handleCalling = () => {
    // setTimeout(() => {
    //   var target_url = window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/patient_calling';
    //   // Open target_url using external browser if in LINE LIFF
    //   if (liff.isInClient()) {
    //     liff.init({
    //       liffId: "1654168131-ngxPjgE7"
    //     }, () => {
    //       liff.openWindow({
    //         url: target_url,
    //         external: true
    //       })
    //     });
    //   }
    //   // Open target_url using in-app browser if in LINE LIFF
    //   else {
    //     window.location.href = target_url;
    //   }
    // }, 3000);
  };

  render() {
    const { data } = this.state;
    return (
      <PatientStep1Styled>
        <div className="header_layout">
          <img
            className="logo_layout"
            src={iconImages['logo.png']}
            alt="logo"
          />
        </div>
        <div className="body_layout">
          <div className="text_layer">
            <div className="textL1">กรุณารอซักครู่ค่ะ</div>
            <div className="textL2">{data && data.message_th}</div>
            <div className="textL3">Please wait a moment</div>
          </div>
          <div className="icon_layer">
            <div className="avatar_layer">
              <AvatarCustom src={iconImages['avatar.png']} />
            </div>
            <img
              className="img_div"
              src={iconImages['call_dot.png']}
              alt="call_dot"
            />
            <div className="round_div"></div>
          </div>
        </div>
        <div className="footer_layout">
          <button>ยกเลิก</button>
          <div className="group_text">
            <div className="footer_text">
              - ทีมงานจะมีการบันทึกการสนทนาบริการ Video Call
              ของท่านเพื่อนำไปใช้พัฒนาและปรับปรุงบริการต่อไป
            </div>
            <div className="footer_text">
              - ความชัดเจนของภาพและเสียง (ในระหว่างการสนทนา)
              ขึ้นอยู่กับความเร็วอินเตอร์เน็ตของผู้ใช้บริการ
            </div>
          </div>
        </div>
      </PatientStep1Styled>
    );
  }
}

const mapStateToProps = (state) => ({
  authRedux: state.auth,
});

const mapDispatchToProps = (dispatch) => ({
  setReduxUserAuth: (data) => dispatch(setReduxUserAuth(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PatientStep1);
