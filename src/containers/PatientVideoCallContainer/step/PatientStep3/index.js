import React from 'react';
import { connect } from 'react-redux';
import { PatientStep3Styled } from './styled';
// import { userService } from 'apiService';
import { iconImages } from 'assets';
import { setReduxUserAuth } from 'actions';
import { ButtonCircle } from 'components';
// import { ROUTE_PATH } from 'helpers';
import theme from 'assets/styledComponent/theme.json';

class PatientStep3 extends React.Component {
  state = {
    api: null,
    muted: false,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.data !== prevState.data) {
      return { data: nextProps.data };
    } else return null;
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.data !== this.state.data) {
      this.handleCalling();
    }
  }

  componentWillUnmount() {
    if (this.state.api) {
      this.state.api.executeCommand('hangup');
      this.state.api.removeEventListener('audioMuteStatusChanged');
    }
  }

  handleCalling = () => {
    const { data } = this.state;
    const domain = data.domain;
    const options = {
      userInfo: {
        displayName: data.displayName,
      },
      roomName: data.roomName,
      width: '100%',
      height: '100%',
      interfaceConfigOverwrite: {
        DEFAULT_REMOTE_DISPLAY_NAME: 'iMeet user',
        DEFAULT_LOCAL_DISPLAY_NAME: 'me',
        SHOW_JITSI_WATERMARK: false,
        SHOW_WATERMARK_FOR_GUESTS: false,
        JITSI_WATERMARK_LINK: 'https://imeet.invitrace.app',
        GENERATE_ROOMNAMES_ON_WELCOME_PAGE: false,
        DISPLAY_WELCOME_PAGE_CONTENT: false,
        APP_NAME: 'iMeet',
        NATIVE_APP_NAME: 'iMeet',
        PROVIDER_NAME: 'Invitrace',
        TOOLBAR_BUTTONS: ['filmstrip', 'tileview'],
        LOCAL_THUMBNAIL_RATIO: 1,
        MOBILE_APP_PROMO: false, // Allow WebApp version on mobile
        SUPPORT_URL: 'https://www.invitrace.app/',
        RECENT_LIST_ENABLED: false,
      },
      parentNode: document.querySelector('#meet'),
    };

    this.setState(
      {
        api: new window.JitsiMeetExternalAPI(domain, options),
      },
      () => {
        this.state.api.addEventListeners({
          audioMuteStatusChanged: this.audioMuteStatusListener,
        });
      }
    );
  };

  audioMuteStatusListener = (event) => {
    this.setState({ muted: event.muted });
  };

  handleClickMute = () => {
    // this.state.api.executeCommand('toggleAudio');
  };

  handleClickHangup = () => {
    const { handleClickStep } = this.props;
    if (this.state.api) this.state.api.executeCommand('hangup');
    let data = {
      visit_state: 'PATIENT_WAITING',
      message_th: 'ท่านกำลังเป็นคิวถัดไป',
    };
    handleClickStep(1, data);
    // this.props.propsHistory.push(ROUTE_PATH.PATIENT_SUMMARY);
  };

  handleClickChat = () => {
    // this.state.api.executeCommand('toggleChat');
  };

  render() {
    return (
      <PatientStep3Styled>
        <div className="video_call_container">
          <div id="meet" className="video_container" />
        </div>
        <div className="action_call_container">
          <div className="action_wrap">
            <ButtonCircle
              onClick={this.handleClickMute}
              bg={
                this.state.muted ? theme.COLORS.PURPLE_1 : theme.COLORS.WHITE_2
              }
              color={
                this.state.muted ? theme.COLORS.WHITE_1 : theme.COLORS.BLACK_1
              }
              radius="16px"
              imgNode={
                <img
                  src={iconImages[this.state.muted ? 'mute.png' : 'mic.png']}
                  style={{ width: 14 }}
                  alt="logo_mute"
                />
              }
              text={this.state.muted ? 'ปิดเสียง' : 'เปิดเสียง'}
            />
            <ButtonCircle
              onClick={this.handleClickHangup}
              diameter="65px"
              bg={theme.COLORS.RED_1}
              imgNode={
                <img
                  src={iconImages['phone.png']}
                  style={{ width: 40 }}
                  alt="phone"
                />
              }
            />
            <ButtonCircle
              onClick={this.handleClickChat}
              radius="16px"
              imgNode={
                <img
                  src={iconImages['chat.png']}
                  style={{ width: 27 }}
                  alt="chat"
                />
              }
              text="ส่งข้อความ"
            />
          </div>
          <div className="detail_wrap">
            <div className="col_right">
              สัญญาณ :
              <img src={iconImages['wave_2.png']} alt="wave_2" />
            </div>
          </div>
        </div>
      </PatientStep3Styled>
    );
  }
}

const mapStateToProps = (state) => ({
  authRedux: state.auth,
});

const mapDispatchToProps = (dispatch) => ({
  setReduxUserAuth: (data) => dispatch(setReduxUserAuth(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PatientStep3);
