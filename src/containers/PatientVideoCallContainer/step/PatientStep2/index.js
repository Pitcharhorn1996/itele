import React from 'react';
import { connect } from 'react-redux';
import { PatientStep2Styled } from './styled';
// import { userService } from 'apiService';
import { iconImages } from 'assets';
import { setReduxUserAuth } from 'actions';
import { AvatarCustom } from 'components';

// const liff = window.liff;

class PatientStep2 extends React.Component {
  state = {
    provider_avatar: iconImages['nurse.png'],
    provider_title: 'พยาบาลวิชาชีพ',
    provider_name: 'อัญญา ภัทรโสภณภักดี',
    message: 'ท่านกำลังเป็นคิวถัดไป',
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.data !== prevState.data) {
      return { data: nextProps.data };
    } else return null;
  }

  componentDidMount() {}

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.data !== this.state.data) {
      this.fechData();
    }
  }

  fechData = () => {
    const { data } = this.props;
    this.setState({
      provider_avatar: data.provider_avatar,
      provider_title: data.provider_title,
      provider_name: data.provider_name,
      message: data.message_th,
    });
  };

  handleCalling = () => {
    // setTimeout(() => {
    //   var target_url = window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/patient_calling';
    //   // Open target_url using external browser if in LINE LIFF
    //   if (liff.isInClient()) {
    //     liff.init({
    //       liffId: "1654168131-ngxPjgE7"
    //     }, () => {
    //       liff.openWindow({
    //         url: target_url,
    //         external: true
    //       })
    //     });
    //   }
    //   // Open target_url using in-app browser if in LINE LIFF
    //   else {
    //     window.location.href = target_url;
    //   }
    // }, 3000);
  };

  render() {
    const {
      provider_avatar,
      provider_title,
      provider_name,
      message,
    } = this.state;
    return (
      <PatientStep2Styled provider_avatar={provider_avatar}>
        <div className="header_layout">
          <img
            className="logo_layout"
            src={iconImages['logo.png']}
            alt="logo"
          />
        </div>
        <div className="body_layout">
          <div className="text_layer">
            <div className="textL1">กรุณารอสักครู่ค่ะ</div>
            <div className="textL2">{message}</div>
            <div className="textL3">Please wait a moment</div>
          </div>
          <div className="icon_layer">
            <div className="avatar_layer">
              <AvatarCustom src={iconImages['avatar.png']} />
            </div>
            <img
              className="img_div"
              src={iconImages['call_dot.png']}
              alt="call_dot"
            />
            <div className="round_div"></div>
            <div className="name_layer">{provider_name}</div>
            <div className="position_layer">{provider_title}</div>
          </div>
        </div>
        <div className="footer_layout">
          <button>ยกเลิก</button>
          <div className="group_text">
            <div className="footer_text">
              - ทีมงานจะมีการบันทึกการสนทนาบริการ Video Call
              ของท่านเพื่อนำไปใช้พัฒนาและปรับปรุงบริการต่อไป
            </div>
            <div className="footer_text">
              - ความชัดเจนของภาพและเสียง (ในระหว่างการสนทนา)
              ขึ้นอยู่กับความเร็วอินเตอร์เน็ตของผู้ใช้บริการ
            </div>
          </div>
        </div>
      </PatientStep2Styled>
    );
  }
}

const mapStateToProps = (state) => ({
  authRedux: state.auth,
});

const mapDispatchToProps = (dispatch) => ({
  setReduxUserAuth: (data) => dispatch(setReduxUserAuth(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PatientStep2);
