import styled from 'styled-components';
import { iconImages } from 'assets';

export const PatientStep2Styled = styled.div`
  /*===============================================
 Container 
===============================================*/

  /*===============================================
Theme 
===============================================*/
  height: 100%;
  .header_layout {
    text-align: center;
    .logo_layout {
      width: 152px;
      padding: 20px 0px;
    }
  }
  .body_layout {
    height: calc(100% - 229px);
    overflow: auto;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    flex-direction: column;
    text-align: center;
    .text_layer {
      margin-bottom: 30px;
      .textL1 {
        font-weight: 400;
        font-size: ${({ theme }) => theme.FONTS.SIZE_3};
        color: ${({ theme }) => theme.COLORS.GREEN_1};
      }
      .textL2 {
        font-weight: 500;
        font-size: ${({ theme }) => theme.FONTS.SIZE_7};
        color: ${({ theme }) => theme.COLORS.GREEN_1};
        line-height: 1.3;
      }
      .textL3 {
        font-weight: 400;
        font-size: ${({ theme }) => theme.FONTS.SIZE_3};
        color: ${({ theme }) => theme.COLORS.PURPLE_1};
      }
    }
    .icon_layer {
      width: 100%;
      display: flex;
      flex-direction: column;
      align-items: center;
      .avatar_layer {
        margin-bottom: 10px;
      }
      .img_div {
        width: 11px;
        margin-bottom: 10px;
      }
      .round_div {
        width: 134px;
        height: 134px;
        border-radius: 50%;
        border: 7px solid ${({ theme }) => theme.COLORS.PURPLE_1};
        overflow: hidden;
        margin: auto;
        background-image: url(${({ provider_avatar }) =>
          provider_avatar ? provider_avatar : iconImages['nurse.png']});
        background-position: center;
        background-size: cover;
        background-repeat: no-repeat;
        margin-bottom: 10px;
      }
      .name_layer {
        font-weight: 700;
        font-size: ${({ theme }) => theme.FONTS.SIZE_3};
        color: ${({ theme }) => theme.COLORS.PURPLE_1};
      }
      .position_layer {
        font-weight: 400;
        font-size: ${({ theme }) => theme.FONTS.SIZE_3};
        color: ${({ theme }) => theme.COLORS.PURPLE_1};
      }
    }
  }
  .footer_layout {
    text-align: center;
    padding: 20px 0px;
    button {
      padding: 8px 25px;
      border: 1px solid ${({ theme }) => theme.COLORS.GREEN_1};
      border-radius: 16px;
      background: none;
      font-family: prompt_medium;
      color: ${({ theme }) => theme.COLORS.GREEN_1};
      font-weight: 400;
      font-size: ${({ theme }) => theme.FONTS.SIZE_3};
    }
    .group_text {
      margin-top: 15px;
      .footer_text {
        text-align: left;
        font-family: prompt_medium;
        color: ${({ theme }) => theme.COLORS.GREY_7};
        font-weight: 400;
        font-size: ${({ theme }) => theme.FONTS.SIZE_0};
        white-space: nowrap;
        padding: 0px 18px;
        overflow: hidden;
        text-overflow: ellipsis;
      }
    }
  }
`;
