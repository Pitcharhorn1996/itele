import React from 'react';
import { connect } from 'react-redux';
import { PatientVideoCallContainerStyled } from './styled';
// import { userService } from 'apiService';
// import { iconImages } from 'assets';
import { setReduxUserAuth } from 'actions';
import mqtt from 'mqtt';
import { ROUTE_PATH } from 'helpers';
import {
  PatientStep1,
  PatientStep2,
  PatientStep3,
  PatientStepNoStep,
} from './step';

class PatientVideoCallContainer extends React.Component {
  state = {};

  componentDidMount() {
    this.handleClickStep(1);
    this.connectMqtt();
  }

  componentWillUnmount() {
    const { authRedux } = this.props;
    const client = this.mqttService;
    client.publish(
      `visits/${authRedux.mqtt.client_id}/client_status`,
      'DISCONNECT',
      {
        retain: true,
      }
    );
    client.end();
  }

  connectMqtt = () => {
    const { authRedux } = this.props;
    console.log('person', authRedux);
    this.mqttService = mqtt.connect(authRedux.mqtt.url, {
      clientId: authRedux.mqtt.client_id,
      username: authRedux.mqtt.username,
      password: authRedux.device_id,
      will: {
        topic: `visits/${authRedux.mqtt.client_id}/client_status`,
        message: 'OFFLINE',
        retain: true,
      },
    });
    const client = this.mqttService;
    client.on('connect', function () {
      client.publish(
        `visits/${authRedux.mqtt.client_id}/client_status`,
        'ONLINE',
        {
          retain: true,
        }
      );
      client.subscribe(`visits/${authRedux.mqtt.client_id}/state`, function (
        err
      ) {
        if (!err) {
          // client.publish('presence', 'Hello mqtt');
        }
      });
      client.subscribe(
        `visits/${authRedux.mqtt.client_id}/vdo_conference`,
        function (err) {
          if (!err) {
            // client.publish('presence', 'Hello mqtt');
          }
        }
      );
    });

    client.on('message', function (topic, message, packet) {
      // message is Buffer
      // console.log(topic,message.toString(),packet);
      // client.end();
      handleRenderStep(JSON.parse(message));
    });

    const handleRenderStep = (e) => {
      switch (e.visit_state) {
        case 'PATIENT_WAITING':
          this.handleClickStep(1, e);
          break;
        case 'PATIENT_WAITING_NURSE':
          this.handleClickStep(2, e);
          break;
        case 'PATIENT_WAITING_DOCTOR':
          this.handleClickStep(2, e);
          break;
        case 'PATIENT_WAITING_PHARMACY':
          this.handleClickStep(1, e);
          break;
        case 'PATIENT_CALLING_NURSE':
          this.handleClickStep(3, e);
          break;
        case 'PATIENT_CALLING_DOCTOR':
          this.handleClickStep(3, e);
          break;
        case 'PATIENT_FINISHED_DOCTOR':
          this.handleClickStep(1, e);
          break;
        case 'PATIENT_VISIT_SUMMARY':
          this.props.history.push(ROUTE_PATH.PATIENT_SUMMARY);
          break;
        default:
          if (e && e.domain) {
            this.handleClickStep(3, e);
          } else {
            this.handleClickStep();
          }
          break;
      }
    };
  };

  handleClickStep = (key, data) => {
    switch (key) {
      case 1:
        this.setState({
          renderStep: <PatientStep1 data={data} />,
        });
        break;
      case 2:
        this.setState({
          renderStep: <PatientStep2 data={data} />,
        });
        break;
      case 3:
        this.setState({
          renderStep: (
            <PatientStep3
              data={data}
              handleClickStep={this.handleClickStep}
              propsHistory={this.props.history}
            />
          ),
        });
        break;
      default:
        this.setState({
          renderStep: <PatientStepNoStep />,
        });
        break;
    }
  };

  render() {
    const { renderStep } = this.state;
    return (
      <PatientVideoCallContainerStyled>
        {renderStep}
      </PatientVideoCallContainerStyled>
    );
  }
}

const mapStateToProps = (state) => ({
  authRedux: state.auth,
});

const mapDispatchToProps = (dispatch) => ({
  setReduxUserAuth: (data) => dispatch(setReduxUserAuth(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PatientVideoCallContainer);
