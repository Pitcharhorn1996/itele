import React from 'react';
import { connect } from 'react-redux';
import { PatientRatingContainerStyled } from './styled';
// import { userService } from 'apiService';
import { iconImages } from 'assets';
import { setReduxUserAuth } from 'actions';
import { Block, TextAreaCustom, Button } from 'components';
import theme from 'assets/styledComponent/theme.json';
import { ROUTE_PATH } from 'helpers';

class PatientRatingContainer extends React.Component {
  state = {};

  handleClickSkip = () => {
    this.props.history.push(ROUTE_PATH.PATIENT_THANK);
  };

  render() {
    return (
      <PatientRatingContainerStyled>
        <div className="main_con">
          <div className="header_layout">ความพึงพอใจในการรับบริการ</div>
          <div className="body_layout">
            <Block theme_block_standard>
              <div className="text1">ท่านพึงพอใจในการรับบริการหรือไม่</div>
              <div className="text2">Service satisaction rating</div>
              {/* <div className="text2">Steeeeeeeeeeeeeeeeeeeeeeeep</div> */}
              <img
                className="emotion3"
                src={iconImages['emotion3.png']}
                alt="emotion3"
              />
            </Block>
            <div className="div_margin" />
            <Block theme_block_standard>
              <div className="text3">คำแนะนำเพิ่มเติม / Suggestion</div>
              <div className="box_area">
                <TextAreaCustom theme_standard />
              </div>
            </Block>
            {/* <div className="box_button">
              <div className="b1_layer margin">
                <Button
                  theme_green
                  bg={theme.COLORS.GREY_5}
                  color={theme.COLORS.GREEN_1}
                  onClick={this.handleClickSkip}
                >
                  ข้าม
                </Button>
              </div>
              <div className="b1_layer">
                <Button
                  theme_green
                  bg={theme.COLORS.GREEN_1}
                  color={theme.COLORS.WHITE_1}
                  onClick={this.handleClickSkip}
                >
                  ยืนยัน
                </Button>
              </div>
            </div> */}
          </div>
          <div className="footer_layout">
            <div className="b1_layer margin">
              <Button
                theme_green
                bg={theme.COLORS.GREY_5}
                color={theme.COLORS.GREEN_1}
                onClick={this.handleClickSkip}
              >
                ข้าม
                </Button>
            </div>
            <div className="b1_layer">
              <Button
                theme_green
                bg={theme.COLORS.GREEN_1}
                color={theme.COLORS.WHITE_1}
                onClick={this.handleClickSkip}
              >
                ยืนยัน
                </Button>
            </div>
          </div>
        </div>
      </PatientRatingContainerStyled>
    );
  }
}

const mapStateToProps = (state) => ({
  authRedux: state.auth,
});

const mapDispatchToProps = (dispatch) => ({
  setReduxUserAuth: (data) => dispatch(setReduxUserAuth(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PatientRatingContainer);
