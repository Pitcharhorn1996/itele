import styled from 'styled-components';

export const PatientRatingContainerStyled = styled.div`
  /*===============================================
 Container 
===============================================*/
height: 100%;
  /*===============================================
Theme 
===============================================*/
  .main_con {
    height: 100%;
    background: ${({ theme }) => theme.COLORS.GREY_5};
    .header_layout {
      font-weight: 700;
      font-size: ${({ theme }) => theme.FONTS.SIZE_4};
      color: ${({ theme }) => theme.COLORS.GREEN_1};
      background: ${({ theme }) => theme.COLORS.GREY_1};
      display: flex;
      justify-content: center;
      padding: 24px 0px 24px 0px;
    }
    .body_layout {
      height: calc(100% - 175px);
      overflow: auto;
      padding: 20px;
      .text1 {
        text-align: center;
        font-weight: 700;
        font-size: ${({ theme }) => theme.FONTS.SIZE_4};
        color: ${({ theme }) => theme.COLORS.GREEN_1};
      }
      .text2 {
        text-align: center;
        font-weight: 400;
        font-size: ${({ theme }) => theme.FONTS.SIZE_3};
        color: ${({ theme }) => theme.COLORS.GREEN_1};
        margin-bottom: 27px;
      }
      .text3 {
        font-weight: 700;
        font-size: ${({ theme }) => theme.FONTS.SIZE_4};
        color: ${({ theme }) => theme.COLORS.GREEN_1};
      }
      .emotion3 {
        width: 100%;
      }
      .box_area {
        margin-top: 10px;
      }
      .div_margin {
        margin-bottom: 16px;
      }
      .box_button {
        margin-top: 40px;
        display: flex;
        justify-content: center;
        .b1_layer {
          width: 50%;
          &.margin {
            margin-right: 10px;
          }
        }
      }
    }
    .footer_layout {
        padding: 0px 20px;
        display: flex;
        justify-content: center;
        .b1_layer {
          width: 50%;
          &.margin {
            margin-right: 10px;
          }
        }
    }
  }

`;
