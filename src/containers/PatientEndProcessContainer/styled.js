import styled from 'styled-components';

export const PatientEndProcessContainerStyled = styled.div`
  /*===============================================
 Container 
===============================================*/

  /*===============================================
Theme 
===============================================*/
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  .container {
    color: ${({ theme }) => theme.COLORS.BLACK_1};
    font-family: prompt_regular;
    font-size: ${({ theme }) => theme.FONTS.SIZE_5};
    .header_layout {
      margin-bottom: 15px;
    }
    text-align: center;
  }
`;
