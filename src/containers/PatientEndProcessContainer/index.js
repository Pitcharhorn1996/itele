import React from 'react';
import { connect } from 'react-redux';
import { PatientEndProcessContainerStyled } from './styled';
import { iconImages } from 'assets';
import { setReduxUserAuth } from 'actions';

class PatientEndProcessContainer extends React.Component {
  state = {};

  componentDidMount() {
    setTimeout(() => {
      this.props.history.push('/');
    }, 2000);
  }

  render() {
    return (
      <PatientEndProcessContainerStyled>
        <div className="container">
          <div className="header_layout">
            <img className="logo" src={iconImages['logo.png']} alt="logo" />
          </div>
          ขอบคุณสำหรับการใช้บริการค่ะ
        </div>
      </PatientEndProcessContainerStyled>
    );
  }
}

const mapStateToProps = (state) => ({
  authRedux: state.auth,
});

const mapDispatchToProps = (dispatch) => ({
  setReduxUserAuth: (data) => dispatch(setReduxUserAuth(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PatientEndProcessContainer);
