import React from 'react';
import { connect } from 'react-redux';
import { PatientFinishContainerStyled } from './styled';
// import { userService } from 'apiService';
import { iconImages } from 'assets';
import { setReduxUserAuth } from 'actions';
import { HeaderStatus, BoxDoctor, DivCustom, Button } from 'components';

class PatientFinishContainer extends React.Component {
  state = {};

  render() {
    return (
      <PatientFinishContainerStyled>
        <div className="header_layout">
          <HeaderStatus padding="14px 15px">
            <div className="layer_1">Call end</div>
            <div className="layer_2">14.23 mins</div>
          </HeaderStatus>
        </div>
        <div className="body_layout">
          <div className="doc_layout">
            <BoxDoctor />
          </div>
          <div className="medi_layout">
            <div className="medi_title">Medicine</div>
            <div className="medi_item_wrap">
              <DivCustom
                theme_standard
                src={iconImages['boplus.png']}
                text="Dr.Boplus TABLET (250 mg)"
              />
            </div>
            <div className="medi_item_wrap">
              <DivCustom
                theme_standard
                src={iconImages['vitamin.png']}
                text="Vitamin D3 (10,000 lu) Caps"
              />
            </div>
          </div>
        </div>
        <div className="footer_layout">
          <div className="button_layout">
            <Button theme_full_width>Next</Button>
          </div>
        </div>
      </PatientFinishContainerStyled>
    );
  }
}

const mapStateToProps = (state) => ({
  authRedux: state.auth,
});

const mapDispatchToProps = (dispatch) => ({
  setReduxUserAuth: (data) => dispatch(setReduxUserAuth(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PatientFinishContainer);
