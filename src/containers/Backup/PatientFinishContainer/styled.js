import styled from 'styled-components';

export const PatientFinishContainerStyled = styled.div`
  /*===============================================
 Container 
===============================================*/

  /*===============================================
Theme 
===============================================*/
  width: 100%;
  height: 100%;
  background: ${({ theme }) => theme.COLORS.GREY_1};
  .header_layout {
    .layer_1 {
      font-size: ${({ theme }) => theme.FONTS.SIZE_4};
      margin-bottom: 2px;
    }
    .layer_2 {
      font-size: ${({ theme }) => theme.FONTS.SIZE_7};
    }
  }
  .body_layout {
    height: calc(100% - 260px);
    padding: 54px 24px 24px 24px;
    overflow: auto;
    .doc_layout {
      margin-bottom: 37px;
    }
    .medi_layout {
      .medi_title {
        font-family: prompt_medium;
        margin-bottom: 16px;
      }
      .medi_item_wrap {
        margin-bottom: 10px;
      }
    }
  }
  .footer_layout {
    padding: 24px;
    .button_layout {
    }
  }
`;
