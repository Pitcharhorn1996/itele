import styled from 'styled-components';

export const PatientConnectingContainerStyled = styled.div`
  /*===============================================
 Container 
===============================================*/

  /*===============================================
Theme 
===============================================*/
  width: 100%;
  height: 100%;
  background: ${({ theme }) => theme.COLORS.GREEN_3};
  .header_layout {
    .layer_1 {
      font-size: ${({ theme }) => theme.FONTS.SIZE_5};
    }
  }
  .body_layout {
    height: calc(100% - 188px);
    padding-top: 44px;
    align-items: center;
    display: flex;
    flex-direction: column;

    .num_layer {
      font-weight: 400;
      font-size: ${({ theme }) => theme.FONTS.SIZE_87};
      color: ${({ theme }) => theme.COLORS.GREEN_1};
      line-height: 84px;
    }
    .txt_layer {
      font-weight: 400;
      font-size: ${({ theme }) => theme.FONTS.SIZE_3};
      color: ${({ theme }) => theme.COLORS.GREEN_1};
      margin-bottom: 60px;
    }
    .img_layer {
      display: flex;
      justify-content: center;
      align-items: center;
      margin-bottom: 60px;
      .avatar_layout {
        margin-right: 10px;
      }
      .dot_img_1 {
        width: 70px;
        height: 12px;
        margin-right: 10px;
      }
      .hospital {
        width: 55px;
        height: 55px;
      }
    }
    .txt_layer2 {
      font-weight: 400;
      font-size: ${({ theme }) => theme.FONTS.SIZE_4};
      color: ${({ theme }) => theme.COLORS.GREEN_1};
      margin-bottom: 23px;
    }
  }
  .footer_layout {
    text-align: center;
    padding: 24px;
    button {
      padding: 11px 21px;
      background: ${({ color, theme }) =>
        color ? color : theme.COLORS.GREEN_4};
      border: none;
      border-radius: 3px;
      box-shadow: 0px 2px 12px -5px rgba(0, 0, 0, 0.5);
      font-size: ${({ theme }) => theme.FONTS.SIZE_4};
    }
  }
`;
