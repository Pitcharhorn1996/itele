import React from 'react';
import { connect } from 'react-redux';
import { PatientConnectingContainerStyled } from './styled';
// import { userService } from 'apiService';
import { iconImages } from 'assets';
import { setReduxUserAuth } from 'actions';
import { HeaderStatus, AvatarCustom } from 'components';

class PatientConnectingContainer extends React.Component {
  state = {};

  render() {
    return (
      <PatientConnectingContainerStyled>
        <div className="header_layout">
          <HeaderStatus padding="14px 15px">
            <div className="layer_1">Appointments</div>
          </HeaderStatus>
        </div>
        <div className="body_layout">
          <div className="num_layer">3</div>
          <div className="txt_layer">People in front of you</div>
          <div className="img_layer">
            <div className="avatar_layout">
              <AvatarCustom
                src={iconImages['avatar.png']}
                width="100px"
                height="100px"
              />
            </div>
            <img
              className="dot_img_1"
              src={iconImages['dotdot.png']}
              alt="dot"
            />
            <img
              className="hospital"
              src={iconImages['hospital.png']}
              alt="hospital"
            />
          </div>
          <div className="txt_layer2">We’ll be in touch shortly</div>
        </div>
        <div className="footer_layout">
          <button>Cancle</button>
        </div>
      </PatientConnectingContainerStyled>
    );
  }
}

const mapStateToProps = (state) => ({
  authRedux: state.auth,
});

const mapDispatchToProps = (dispatch) => ({
  setReduxUserAuth: (data) => dispatch(setReduxUserAuth(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PatientConnectingContainer);
