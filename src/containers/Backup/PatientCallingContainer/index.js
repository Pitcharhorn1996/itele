import React from 'react';
import { connect } from 'react-redux';
import { PatientCallingContainerStyled } from './styled';
// import { userService } from 'apiService';
import { iconImages } from 'assets';
import { setReduxUserAuth } from 'actions';
import { ButtonCircle } from 'components';

class PatientCallingContainer extends React.Component {
  state = {
    api: null,
    muted: false
  };

  componentDidMount() {
    const domain = 'imeet.invitrace.app';
    const options = {
      userInfo: {
          displayName: 'iTele Patient'
      },
      roomName: 'iTele',
      width: '100%',
      height: '100%',
      interfaceConfigOverwrite: {
        DEFAULT_REMOTE_DISPLAY_NAME: 'iMeet user',
        DEFAULT_LOCAL_DISPLAY_NAME: 'me',
        SHOW_JITSI_WATERMARK: false,
        SHOW_WATERMARK_FOR_GUESTS: false,
        JITSI_WATERMARK_LINK: 'https://imeet.invitrace.app',
        GENERATE_ROOMNAMES_ON_WELCOME_PAGE: false,
        DISPLAY_WELCOME_PAGE_CONTENT: false,
        APP_NAME: 'iMeet',
        NATIVE_APP_NAME: 'iMeet',
        PROVIDER_NAME: 'Invitrace',
        TOOLBAR_BUTTONS: ['filmstrip', 'tileview'],
        LOCAL_THUMBNAIL_RATIO: 1,
        MOBILE_APP_PROMO: false, // Allow WebApp version on mobile
        SUPPORT_URL: 'https://www.invitrace.app/',
        RECENT_LIST_ENABLED: false,
      },
      parentNode: document.querySelector('#meet'),
    };

    this.setState({
      api: new window.JitsiMeetExternalAPI(domain, options)
    }, () => {
      this.state.api.addEventListeners({
        audioMuteStatusChanged: this.audioMuteStatusListener
      });
    })
  }

  componentWillUnmount() {
    this.state.api.removeEventListener('audioMuteStatusChanged');
  }

  audioMuteStatusListener = (event) => {
    this.setState({ muted: event.muted });
  }

  handleClickMute = () => {
    this.state.api.executeCommand('toggleAudio');
  }

  handleClickHangup = () => {
    this.state.api.executeCommand('hangup');
  }

  handleClickChat = () => {
    this.state.api.executeCommand('toggleChat');
  }

  render() {
    return (
      <PatientCallingContainerStyled>
        <div className="video_call_container">
          <div id="meet" className="video_container" />
        </div>
        <div className="action_call_container" >
          <div className="action_wrap">
            <ButtonCircle
              onClick={this.handleClickMute}
              bg={(this.state.muted) ? "red" : ""}
              imgNode={
                <img
                  src={iconImages[(this.state.muted) ? "mute.png" : "unmute.png"]}
                  style={{ width: 21 }}
                  alt="logo_mute"
                />
              }
              text={(this.state.muted) ? "Mute" : ""}
            />
            <ButtonCircle
              onClick={this.handleClickHangup}
              diameter="81px"
              bg="red"
              imgNode={
                <img
                  src={iconImages['phone.png']}
                  style={{ width: 50 }}
                  alt="phone"
                />
              }
            />
            <ButtonCircle
              onClick={this.handleClickChat}
              imgNode={
                <img
                  src={iconImages['chat.png']}
                  style={{ width: 27 }}
                  alt="chat"
                />
              }
              text="chat"
            />
          </div>
          <div className="detail_wrap">
            <div className="col_left">
              <img src={iconImages['time.png']} alt="time" />
              03.52 Mins
            </div>
            <div className="col_right">
              <img src={iconImages['plus.png']} alt="plus" />
              Extra Time
            </div>
          </div>
        </div>
      </PatientCallingContainerStyled>
    );
  }
}

const mapStateToProps = (state) => ({
  authRedux: state.auth,
});

const mapDispatchToProps = (dispatch) => ({
  setReduxUserAuth: (data) => dispatch(setReduxUserAuth(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PatientCallingContainer);
