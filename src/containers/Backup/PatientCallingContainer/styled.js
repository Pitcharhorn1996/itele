import styled from 'styled-components';
import { iconImages } from 'assets';

export const PatientCallingContainerStyled = styled.div`
  /*===============================================
 Container
===============================================*/

  /*===============================================
Theme
===============================================*/
  height: 100%;
  position: relative;
  .video_call_container {
    background-image: url(${ iconImages['bg_calling.png']});
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    height: 100vh;
    width: 100vw;
  }
  .video_container {
    height: 100%;
    width: 100%;
  }
  .action_call_container {
    width: 100%;
    position: absolute;
    bottom: 0;
    .action_wrap {
      display: flex;
      justify-content: space-between;
      align-items: center;
      padding: 0 28px;
    }
    .detail_wrap {
      display: flex;
      margin: 11px 8px 0 8px;
      border-top-left-radius: 12px;
      border-top-right-radius: 12px;
      overflow: hidden;
      .col_left {
        width: 50%;
        padding: 15px 20px;
        background: ${({ theme }) => theme.COLORS.GREEN_1};
        color: ${({ theme }) => theme.COLORS.WHITE_1};
        display: flex;
        justify-content: center;
        align-items: center;
        img {
          width: 20px;
          height: 20px;
          margin-right: 5px;
        }
      }
      .col_right {
        width: 50%;
        padding: 15px 20px;
        background: ${({ theme }) => theme.COLORS.WHITE_1};
        color: ${({ theme }) => theme.COLORS.GREEN_1};
        display: flex;
        justify-content: center;
        align-items: center;
        img {
          width: 12px;
          height: 12px;
          margin-right: 5px;
        }
      }
    }
  }
`;
