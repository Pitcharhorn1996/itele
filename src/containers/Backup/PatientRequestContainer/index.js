import React from 'react';
import { connect } from 'react-redux';
import { PatientRequestContainerStyled } from './styled';
// import { userService } from 'apiService';
// import { iconImages } from 'assets';
import { setReduxUserAuth } from 'actions';
import { HeaderStatus, DayPickker, ActionTime } from 'components';

class PatientRequestContainer extends React.Component {
  state = {};

  render() {
    return (
      <PatientRequestContainerStyled>
        <div className="header_layout">
          <HeaderStatus>Request an Appointment</HeaderStatus>
        </div>
        <div className="body_layout">
          <DayPickker />
        </div>
        <div className="footer_layout">
          <ActionTime />
        </div>
      </PatientRequestContainerStyled>
    );
  }
}

const mapStateToProps = (state) => ({
  authRedux: state.auth,
});

const mapDispatchToProps = (dispatch) => ({
  setReduxUserAuth: (data) => dispatch(setReduxUserAuth(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PatientRequestContainer);
