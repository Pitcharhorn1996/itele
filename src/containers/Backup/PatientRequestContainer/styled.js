import styled from 'styled-components';

export const PatientRequestContainerStyled = styled.div`
  /*===============================================
 Container 
===============================================*/

  /*===============================================
Theme 
===============================================*/
  height: 100%;
  .header_layout {
  }
  .body_layout {
    height: calc(100% - 296px);
    overflow: auto;
    padding-top: 30px;
    background: ${({ theme }) => theme.COLORS.WHITE_1};
  }
  .footer_layout {
    padding: 19px 20px;
  }
`;
