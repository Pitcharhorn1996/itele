import React from 'react';
import { connect } from 'react-redux';
import { PatientRegisterContainerStyled } from './styled';
import { userService } from 'apiService';
import { iconImages } from 'assets';
import { setReduxUserAuth } from 'actions';
import { Button } from 'components';
import { PatientRegisterForm } from 'forms';
import { ROUTE_PATH, generateUID } from 'helpers';
import { toast } from 'react-toastify';

class PatientRegisterContainer extends React.Component {
  state = {};

  handleClickStartService = () => {
    this.props.history.push(ROUTE_PATH.VIDEO_CALL);
  };

  handleClickSuccess = async () => {
    const { setReduxUserAuth } = this.props;
    const {
      reduxForm: { values, syncErrors },
    } = this.props;
    if (typeof syncErrors === 'object') {
      toast.error('กรุณากรอกข้อมูลให้ครบถ้วน', {
        position: toast.POSITION.TOP_RIGHT,
      });
    } else {
      let params = {
        ...values,
        device_id: generateUID(),
      };
      let res = await userService.POST_VISITS(params);
      if (res && res.success) {
        setReduxUserAuth({
          ...res.data,
          device_id: params.device_id,
        });
        this.props.history.push(ROUTE_PATH.VIDEO_CALL);
      }
    }
  };

  render() {
    return (
      <PatientRegisterContainerStyled>
        <div className="main_con">
          <div className="header_layout">
            <img className="logo" src={iconImages['logo.png']} alt="logo" />
          </div>
          <div className="body_layout">
            <PatientRegisterForm />
          </div>
          <div className="footer_layer">
            <div className="button_layer" onClick={this.handleClickSuccess}>
              <Button theme_green>เริ่มรับบริการ</Button>
            </div>
          </div>
        </div>
      </PatientRegisterContainerStyled>
    );
  }
}

const mapStateToProps = (state) => ({
  authRedux: state.auth,
  reduxForm: state.form.PatientRegisterForm,
});

const mapDispatchToProps = (dispatch) => ({
  setReduxUserAuth: (data) => dispatch(setReduxUserAuth(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PatientRegisterContainer);
