import styled from 'styled-components';

export const PatientRegisterContainerStyled = styled.div`
  /*===============================================
 Container 
===============================================*/
  height: 100%;
  /*===============================================
Theme 
===============================================*/
  .main_con {
    height: 94%;
    background: ${({ theme }) => theme.COLORS.GREY_1};
    padding: 20px 24px;
    .header_layout {
      text-align: center;
      margin-bottom: 20px;
      .logo {
        width: 152px
      }
    }
    .body_layout {
      height: calc(100% - 131px);
      overflow: auto;
    }
  }

`;
