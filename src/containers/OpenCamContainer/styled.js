import styled from 'styled-components';

export const OpenCamContainerStyled = styled.div`
  /*===============================================
 Container 
===============================================*/
height: 100%;
  /*===============================================
Theme 
===============================================*/
.head_layout {
  background: ${({ theme }) => theme.COLORS.GREY_1};
  .header_icon {
    text-align: center;
    .logo {
      width: 152px;
      height: 94%;
      padding: 20px 24px;
    } 
  }
}

.body_layout {
  background: ${({ theme }) => theme.COLORS.GREY_1};
  .text_body{
    font-weight: 700;
    font-size: ${({ theme }) => theme.FONTS.SIZE_4};
    color: #808080;
    background: ${({ theme }) => theme.COLORS.GREY_1};
    display: flex;
    justify-content: center;
  }
  .text_1 {
    font-weight: 600;
    font-size: ${({ theme }) => theme.FONTS.SIZE_4};
    color: ${({ theme }) => theme.COLORS.GREEN_1};
    display: flex;
    justify-content: center;
  }
  .text_2 {
    font-weight: 600;
    font-size: ${({ theme }) => theme.FONTS.SIZE_2};
    color: #606060;
    background: ${({ theme }) => theme.COLORS.GREY_1};
    display: flex;
    justify-content: center;
    text-align: center;
  }
  .logo_mic {
    width: 15px;
  }
  .logo_cam {
    width:  35px;
  }
  .logo_opencam {
    width: 300px;
    margin-top: 20px;
    margin-bottom: 20px;
  }
  .flex-container {
    display: flex;
    justify-content: center;
  }
  .flex-container > div {
    //margin: 10px;
    padding: 10px;
  }
  
}

.footer_layout {
  padding: 30px 20px;
}
  
`;
