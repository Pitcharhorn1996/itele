import React  from 'react';
import { connect } from 'react-redux';
import { OpenCamContainerStyled } from './styled';
// import { userService } from 'apiService';
import { iconImages } from 'assets';
import { ROUTE_PATH } from 'helpers';
import {  Button } from 'components';
//import {  getUserMedia } from 'components';


class OpenCamContainer extends React.Component {
  
  //************************* set default button ****************************
  constructor(props){
    super(props)
    this.state = {
        btn: 'เปิดการเข้าถึงไมโครโฟนและกล้อง'
        }
    }
    
  //************************* when click "NEXT" ****************************
  handleClickNext = () => {
    this.props.history.push(ROUTE_PATH.VIDEO_CALL);
    window.localStream.getAudioTracks()[0].stop();
    window.localStream.getVideoTracks()[0].stop();
    // setTimeout(() => {
    //   window.localStream.getAudioTracks()[0].stop();
    //   window.localStream.getVideoTracks()[0].stop();
    // }, 2000);
  };

  //************************* permission ****************************
  handleClickAllow = () => {
    console.log(navigator)
    navigator.getUserMedia(
      {
        video: true, 
        audio: true,
      }, (localMediaStream) => {
        window.localStream = localMediaStream;

        this.setState(prevState =>({
          btn : 'NEXT',
      }))
        
      }, (err) => {
        alert('กรุณาอนุญาตให้เข้าถึงไมโครโฟนและกล้อง')
      });
  };


  render() {
    //************************* change button ****************************
    let button;
    if (this.state.btn === 'NEXT') {
      button = this.handleClickNext;
    } else {
      button = this.handleClickAllow ;
    }
    
    return (
      <OpenCamContainerStyled>

        {/* ******************************* logo header ******************************** */}
        <div className="head_layout">
          <div className="header_icon">
            <img className="logo" src={iconImages['logo.png']} alt="logo" />
          </div>
        </div>


        {/* **************************************** body ***************************************** */}
        <div className="body_layout">
          <div className="text_body" >กรุณา</div>
          <div className="text_1">"เปิดการเข้าถึงไมโครโฟนและกล้อง"</div>
            {/* ******************************* mic & cam pic ******************************** */}
            <div className="flex-container">
              <div><img className="logo_mic" src={iconImages['mic.png']} alt="logo" /></div>
              <div><img className="logo_cam" src={iconImages['cam.png']} alt="logo" /></div>
            </div>
          <div className="text_2">
            บนอุปกรณ์ของท่าน<br></br>
            เพื่อการเข้ารับบริการ Virtual Hospital
          </div>
          {/* ******************************* middle pic ******************************** */}
          <div className="flex-container">
              <img className="logo_opencam" src={iconImages['open_cam.png']} alt="logo" />
          </div>
        </div>


        {/* ******************************* button ******************************** */}
        <div className="footer_layout">
          <div onClick={button}>
            <Button theme_green> { this.state.btn }</Button>
          </div>
        </div>
      </OpenCamContainerStyled>
    );
  }
}


export default connect(

)(OpenCamContainer);
