import styled from 'styled-components';

export const PatientSummaryContainerStyled = styled.div`
  /*===============================================
 Container 
===============================================*/
height: 100%;
  /*===============================================
Theme 
===============================================*/
  .main_con {
    height: 100%;
    background: ${({ theme }) => theme.COLORS.GREY_5};
    .header_layout {
      padding: 20px 0px;
      font-weight: 700;
      font-size: ${({ theme }) => theme.FONTS.SIZE_4};
      color: ${({ theme }) => theme.COLORS.GREEN_1};
      background: ${({ theme }) => theme.COLORS.GREY_1};
      display: flex;
      justify-content: center;
    }
    .body_layout {
      height: calc(100% - 164px);
      overflow: auto;
      padding: 20px;
      .box_number {
        padding: 0px 18px 18px 18px;
        display: flex;
        justify-content: space-between;
        border-bottom: 1px solid ${({ theme }) => theme.COLORS.GREY_6};
        align-items: center;
        .b_left {
          .text_1 {
            font-weight: 700;
            font-size: ${({ theme }) => theme.FONTS.SIZE_3};
            color: ${({ theme }) => theme.COLORS.GREEN_1};
          }
          .text_2 {
            font-weight: 400;
            font-size: ${({ theme }) => theme.FONTS.SIZE_2};
            color: ${({ theme }) => theme.COLORS.GREEN_1};
          }
        }
        .b_right {
          font-weight: 400;
          font-size: ${({ theme }) => theme.FONTS.SIZE_3};
        }
      }
      .box_bill {
        padding: 18px 18px 24px 18px;
        .bill_title {
          font-weight: 700;
          font-size: ${({ theme }) => theme.FONTS.SIZE_4};
          color: ${({ theme }) => theme.COLORS.GREEN_1};
          margin-bottom: 8px;
        }
        .bill_date {
          padding-bottom: 8px;
          border-bottom: 1px solid ${({ theme }) => theme.COLORS.GREY_6};
          .date_text {
            font-weight: 400;
            font-size: ${({ theme }) => theme.FONTS.SIZE_3};
            color: ${({ theme }) => theme.COLORS.PURPLE_1};
          }
          .detail_text {
            font-weight: 400;
            font-size: ${({ theme }) => theme.FONTS.SIZE_2};
            color: ${({ theme }) => theme.COLORS.GREY_7};
          }
        }
      }
      .box_list {
        padding: 0px 18px;
      }
      .div_margin {
        margin-bottom: 8px;
      }
      .box_label {
        font-weight: 700;
        font-size: ${({ theme }) => theme.FONTS.SIZE_4};
        color: ${({ theme }) => theme.COLORS.GREEN_1};
        padding-left: 18px;
        padding-bottom: 15px;
      }
      .div_margin2 {
        margin-bottom: 20px;
      }
      .box_check {
        display: flex;
        margin-bottom: 20px;
        .check_l {
          width: 20px;
          height: 20px;
          display: flex;
          flex-shrink: 0;
          margin-right: 5px;
        }
        .check_r {
          .l_text1 {
            font-weight: 400;
            font-size: ${({ theme }) => theme.FONTS.SIZE_3};
            color: ${({ theme }) => theme.COLORS.GREY_7};
          }
          .l_text2 {
            font-weight: 400;
            font-size: ${({ theme }) => theme.FONTS.SIZE_2};
            color: ${({ theme }) => theme.COLORS.GREY_7};
          }
        }
      }
    }
    .footer_layout {
      padding: 0px 20px;
    }
  }

`;
