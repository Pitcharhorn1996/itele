import React from 'react';
import { connect } from 'react-redux';
import { PatientSummaryContainerStyled } from './styled';
// import { userService } from 'apiService';
import { iconImages } from 'assets';
import { setReduxUserAuth } from 'actions';
import { ROUTE_PATH } from 'helpers';
import { Block, MiniTable, Button } from 'components';

class PatientSummaryContainer extends React.Component {
  state = {};

  handleClickNext = () => {
    this.props.history.push(ROUTE_PATH.PATIENT_RATING);
  };

  render() {
    return (
      <PatientSummaryContainerStyled>
        <div className="main_con">
          <div className="header_layout">สรุปค่าใช้จ่าย</div>
          <div className="body_layout">
            <Block theme_block_non_bottom>
              <div className="box_number">
                <div className="b_left">
                  <div className="text_1">หมายเลขอ้างอิง</div>
                  <div className="text_2">Reference number</div>
                </div>
                <div className="b_right">IT-445223023</div>
              </div>
              <div className="box_bill">
                <div className="bill_title">
                  สรุปค่าใช้บริการ / Total service fees
                </div>
                <div className="bill_date">
                  <div className="date_text">วันที่ 18 เมษายน 2563</div>
                  <div className="detail_text">
                    ผู้ให้คำปรึกษา: พญ. ชาริสา ธนเกียรติโกศล
                  </div>
                </div>
              </div>
              <div className="box_list">
                <MiniTable theme_standard title="รายการ / Lists" data={data} />
              </div>
            </Block>
            <div className="div_margin" />
            <Block theme_block_non_bottom>
              <div className="box_label">คำแนะนำ / Doctor’s Recommendtion</div>
            </Block>
            <div className="div_margin2" />
            <div className="box_check">
              <img
                className="check_l"
                src={iconImages['checkbox.png']}
                alt="checkbox"
              />
              <div className="check_r">
                <div className="l_text1">
                  รับใบเสร็จและสรุปการรับบริการทางอีเมล์
                </div>
                <div className="l_text2">
                  I would like to recieve a receipt and summary consultation
                  report.
                </div>
              </div>
            </div>
          </div>
          <div className="footer_layout">
            <div onClick={this.handleClickNext}>
              <Button theme_green>ถัดไป</Button>
            </div>
          </div>
        </div>
      </PatientSummaryContainerStyled>
    );
  }
}

const data = [
  {
    detail: 'Ibuprofen TAB (400 mg) x 10',
    price: '85.20',
  },
  {
    detail: 'ค่าบริการเวลารวม / Service Fees: 15 นาที',
    price: '500.00',
  },
];
const mapStateToProps = (state) => ({
  authRedux: state.auth,
});

const mapDispatchToProps = (dispatch) => ({
  setReduxUserAuth: (data) => dispatch(setReduxUserAuth(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PatientSummaryContainer);
