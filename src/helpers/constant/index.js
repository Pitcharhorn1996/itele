export const ROUTE_PATH = {
  LOGIN: '/',
  CONDITION: '/condition',
  OPEN_CAM: '/opencamera',
  VIDEO_CALL: '/videocall',
  PATIENT_RATING: '/patient_rating',
  PATIENT_SUMMARY: '/patient_summary',
  PATIENT_THANK: '/patient_thank',
};

export const VALIDATE = {
  REQUIRE: (value) => (!value ? 'required' : ''),
  NUMBER: (value) =>
    value
      ? isNaN(Number(value))
        ? 'Must be a number'
        : undefined
      : 'Must be a number',
  NUMBER_ONLY: (value) =>
    value && isNaN(Number(value)) ? 'Must be a number' : undefined,
  EMAIL: (value) =>
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
      ? 'email invalid'
      : undefined,
  MIN_LENGTH: (value) =>
    value
      ? value.length < 6
        ? `Must be 6 characters or more`
        : undefined
      : `Must be 6 characters or more`,
  MIN_PW_LENGTH: (value) =>
    value
      ? value.length < 8
        ? `Must be 8 characters or more`
        : undefined
      : `Must be 8 characters or more`,
  ID_CARD: (id) => {
    if (id) {
      if (id.length !== 13) return 'กรุณาใส่เลขบัตรใหม่';
      let sum = 0
      for (let i = 0; i < 12; i++)
        sum += parseFloat(id.charAt(i)) * (13 - i);
      if ((11 - (sum % 11)) % 10 !== parseFloat(id.charAt(12)))
        return 'กรุณาใส่เลขบัตรใหม่';
      return undefined;
    } else {
      return 'กรุณาใส่เลขบัตรใหม่';
    }
  },
};

export const IMAGE_PATH = 'http://localhost:8089/static/';
// export const IMAGE_PATH = "https://backend.test.com/static/"
