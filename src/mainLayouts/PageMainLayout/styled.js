import styled from 'styled-components';

export const PageMainLayoutStyled = styled.div`
  /*===============================================
 Container 
===============================================*/

  /*===============================================
Theme 
===============================================*/
  background: ${({ theme }) => theme.COLORS.GREY_1};
  height: 100vh;
`;
