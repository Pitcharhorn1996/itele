import { USER_AUTHEN } from './actionTypes';

export function setReduxUserAuth(data) {
  return {
    type: USER_AUTHEN,
    data
  };
}
